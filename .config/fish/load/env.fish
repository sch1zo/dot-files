setenv EDITOR vim
setenv VISUAL vim

setenv LANGUAGE en_US.UTF-8
setenv LANG en_US.UTF-8
setenv LC_ALL en_US.UTF-8

#setenv PAGER less
#setenv LESS "-iMSx4 -R"

setenv PAGER vimpager
alias less $PAGER
alias zless $PAGER
